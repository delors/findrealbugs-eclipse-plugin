/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin
package views

import extension.ExtensionAnalyses
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.jdt.core._
import org.eclipse.jface.action._
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.text.BadLocationException
import org.eclipse.jface.viewers._
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets._
import org.eclipse.ui._
import org.eclipse.ui.ide.IDE
import org.eclipse.ui.part.ViewPart
import org.eclipse.ui.texteditor.ITextEditor
import java.net.URL

/**
 * This class implements the FindREALBugs report view, which displays the analysis
 * results. It creates and manages the displayed viewer.
 *
 * @author Florian Brandherm
 */
class FindRealBugsReportView extends ViewPart {
    // Viewer that is displayed in the eclipse window
    private var viewer: TreeViewer = null

    // Action that opens the corresponding user Documentation for a
    // Selected report or analysis name
    private var openDocumentationAction: Action = null

    // Action that jumps to the file and line where the bug was found for any selected
    // report
    private var openSourceAction: Action = null

    // Same as openSourceAction, but as a check box
    private var linkWithEditorAction: Action = null

    // Content provider that contains the results in a format that can be used by the
    // TreeViewer.
    private var contentProvider = new ReportViewContentProvider

    /**
     * This is a callback that will allow us to create the viewer and initialize it.
     *
     * @param parent parent Widget
     */
    def createPartControl(parent: Composite) {
        // Create the viewer
        viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
        viewer.setContentProvider(contentProvider)
        viewer.setLabelProvider(new ReportViewLabelProvider());
        viewer.setSorter(new ViewerSorter());
        viewer.setInput(getViewSite());

        // Create the help context id for the viewer's control
        val helpSystem = PlatformUI.getWorkbench().getHelpSystem()
        helpSystem.setHelp(viewer.getControl(),
            "de.tud.cs.st.bat.findrealbugs.plugin.viewer");

        // Set up actions ...
        makeActions

        // ... and hook them up to the view.
        hookContextMenu
        hookDoubleClickAction
        hookSelectionChangedAction
        contributeToActionBars
    }

    /**
     * Sets the results of a FindREALBugs analysis for the tree view.
     *
     * @param results Results of an analysis.
     * @param project The IJavaProject that was analyzed.
     */
    def setResults(
        results: Array[(String, FindRealBugs.AnalysisReports)],
        project: IJavaProject) {

        // Convert the FindRealBugs output-format into the tree structure internal one
        val unifiedResults =
            for ((analysisName, reports) ← results) yield {
                (analysisName, reports.map(ReportData(_, project)))
            }

        // Set/update the analysis results for the given project in the content provider
        contentProvider.setResults(project, unifiedResults)

        // Refresh the viewer (notifies the eclipse graphical user interface of a change)
        viewer.refresh()
    }

    /**
     * Adds the context menu to the viewer.
     */
    private def hookContextMenu() {
        // Create new context menuManager
        val menuMgr: MenuManager = new MenuManager("#PopupMenu")
        menuMgr.setRemoveAllWhenShown(true)
        menuMgr.addMenuListener(new IMenuListener() {
            def menuAboutToShow(manager: IMenuManager) {
                // Generate context menu entries
                fillContextMenu(manager)
            }
        })

        // Create menu and register
        val menu: Menu = menuMgr.createContextMenu(viewer.getControl())
        viewer.getControl().setMenu(menu)
        getSite().registerContextMenu(menuMgr, viewer)
    }

    /**
     * Adds actions to local tool bar.
     */
    private def contributeToActionBars() {
        val bars: IActionBars = getViewSite().getActionBars
        fillLocalToolBar(bars.getToolBarManager)
    }

    /**
     * Adds actions to context menu.
     *
     * @param manager parent IMenuManager object
     */
    private def fillContextMenu(manager: IMenuManager) {
        manager add openSourceAction
        manager add openDocumentationAction
        manager add new Separator
    }

    /**
     * Add actions to the local tool bar.
     *
     * @param manager parent IToolbarManager object
     */
    private def fillLocalToolBar(manager: IToolBarManager) {
        manager add openDocumentationAction
        manager add linkWithEditorAction
        manager add new Separator
    }

    /**
     * Defines the actions that can be invoked by the user by double clicking the tree
     * view items, or clicking the tool bar buttons or context menu items.
     */
    private def makeActions() {

        /**
         * This action results in the opening of a web browser containing the user
         * documentation if a string or ReportData object is selected in the view.
         */
        openDocumentationAction = new Action() {
            /**
             * This is invoked by Eclipse when the action is triggered.
             */
            override def run() {
                val selection: ISelection = viewer.getSelection()

                // Acquire the object that was double clicked in the viewer
                val obj: Object =
                    (selection.asInstanceOf[IStructuredSelection]).getFirstElement()

                obj match {
                    // If it's a string, the user clicked on an analysis name.
                    // Proceed by opening a web browser window to the documentation wiki.
                    case (project: IJavaProject, analysisName: String) ⇒
                        openDocumentation(analysisName)

                    // If it's a ReportData object, the user clicked on an individual
                    // report. Proceed by opening a web browser window to the
                    // documentation of the parent analysis.
                    case (project: IJavaProject, report: ReportData) ⇒
                        openDocumentation(viewer.getContentProvider().
                            asInstanceOf[ReportViewContentProvider].
                            getAnalysisName(project, report))

                    case _ ⇒ // Nothing was selected
                }
            }

            /**
             * Opens the user documentation wiki page of a given analysis inside a
             * web browser window.
             *
             * @param analysisName name of the analysis as in the wiki
             */
            def openDocumentation(analysisName: String) {
                val browser = PlatformUI.getWorkbench().getBrowserSupport().
                    createBrowser("FindREALBugs documentation")
                browser.openURL(
                    new URL(ExtensionAnalyses.getDocumentationURL(analysisName).
                        getOrElse(FindRealBugsCLI.wikiUrlPrefix + analysisName)))
            }

        }

        openDocumentationAction.setText("Show documentation")
        openDocumentationAction.setToolTipText(
            "Show the user documentation for this analysis")
        openDocumentationAction.setImageDescriptor(PlatformUI.getWorkbench().
            getSharedImages().getImageDescriptor(ISharedImages.IMG_LCL_LINKTO_HELP))

        /**
         * This action results in the opening of a source file if a ReportData object
         * is selected in the viewer.
         */
        class OpenSourceAction extends Action {

            /**
             * This is invoked by eclipse when the action is triggered.
             */
            override def run() {
                val selection: ISelection = viewer.getSelection()

                // Acquire the object that was double clicked in the viewer
                val obj: Object =
                    (selection.asInstanceOf[IStructuredSelection]).getFirstElement()

                obj match {
                    // If it's a ReportData object, the user clicked on an individual
                    // report. Proceed by jumping to the corresponding code in an editor.
                    case (project: IJavaProject, report: ReportData) ⇒
                        openFileAndJumpToLineIfPossible(project, report)

                    case _ ⇒ // Do nothing
                }
            }

            /**
             * Tries opening the file associated with the class name inside report in an
             * editor and jumping to the line specified in report.
             *
             * If the class name is unknown, no editor will be opened and the user will
             * be notified with a message box. If the class was defined inside a library
             * archive file (.jar/.zip) no editor will be opened and the user will be
             * notified with a message box.
             *
             * @param project the java project that the ReportData Object belongs to
             * @param report ReportData object, whose referenced bug should be shown
             * in an editor
             */
            def openFileAndJumpToLineIfPossible(
                project: IJavaProject,
                report: ReportData) {

                // If the class name is unknown, we can't jump to the code,
                // so abort and show an error.
                if (report.className == null) {
                    showMessage("error: Class name of this bug is unknown.")
                    return
                }

                val classType =
                    ResourceLocationUtility.findType(project, report.className)

                if (classType == null) {
                    showMessage("error: Class can't be found by the project.")
                    return
                }

                val path = classType.getPath()
                val page = PlatformUI.getWorkbench().
                    getActiveWorkbenchWindow().getActivePage()
                val file = ResourcesPlugin.getWorkspace().
                    getRoot().getFile(path)

                // If the class was defined in a library archive, we cannot open it,
                // so abort and show an error.
                if (file.getName().endsWith(".jar") ||
                    file.getName().endsWith(".zip")) {
                    showMessage("Can't open .java file for class defined in .jar/.zip"+
                        " archive\n"+file.getProjectRelativePath.toString)
                    return
                }

                // Open editor
                val editor = IDE.openEditor(page, file).asInstanceOf[ITextEditor]
                val document = editor.getDocumentProvider().
                    getDocument(editor.getEditorInput())

                // If the document is available, try jumping to the line the report points
                // to if the line number is available.
                if (document != null) {
                    try {
                        val lineNumberOption = report.linenumber
                        if (lineNumberOption.isDefined) {
                            // Subtracting 1 to convert the line number from 1-based (report's
                            // format) to 0-based (Eclipse's format).
                            val lineInfo =
                                document.getLineInformation(lineNumberOption.get - 1)
                            if (lineInfo != null) {
                                editor.selectAndReveal(lineInfo.getOffset(),
                                    lineInfo.getLength())
                            }
                        }
                    } catch {
                        case _: BadLocationException ⇒
                        // Ignore; if it didn't work, we can't do anything about it.
                    }
                }
            }
        }

        openSourceAction = new OpenSourceAction
        openSourceAction.setText("Jump to source")
        openSourceAction.setToolTipText("Jump to the location this bug was found at")
        openSourceAction.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
            getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED))

        linkWithEditorAction = new OpenSourceAction() {
            override def getStyle(): Int = {
                return IAction.AS_CHECK_BOX
            }
        }
        linkWithEditorAction.setText("Link with Editor")
        linkWithEditorAction.setToolTipText("Always jump to the location the selected "+
            "bug was found at")
        linkWithEditorAction.setImageDescriptor(PlatformUI.getWorkbench().
            getSharedImages().getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED))
    }

    /**
     * Hooks up the double click actions with the viewer.
     */
    private def hookDoubleClickAction() {
        viewer.addDoubleClickListener(new IDoubleClickListener() {
            def doubleClick(event: DoubleClickEvent) {
                val selection = event.getSelection().asInstanceOf[IStructuredSelection].
                    getFirstElement()
                selection match {
                    case (_: IJavaProject, analysisName: String) ⇒
                        openDocumentationAction.run()
                    case (_: IJavaProject, report: ReportData) ⇒
                        openSourceAction.run()
                    case _ ⇒ // Ignore
                }
            }
        })
    }

    /**
     * Hooks up the actions that are executed if the selection changes in the viewer.
     */
    private def hookSelectionChangedAction() {
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            def selectionChanged(event: SelectionChangedEvent) {
                val selection = event.getSelection().asInstanceOf[IStructuredSelection].
                    getFirstElement()
                selection match {
                    case (_, report: ReportData) ⇒
                        if (linkWithEditorAction.isChecked()) {
                            linkWithEditorAction.run()
                        }
                    case _ ⇒ // Ignore
                }
            }
        })
    }

    /**
     * Helper function to show a message box easily. Opens a message box with specified
     * test and title "FindREALBugs" to the user.
     *
     * @param message message that is to be displayed
     */
    private def showMessage(message: String) {
        MessageDialog.openInformation(
            viewer.getControl().getShell(),
            "FindREALBugs",
            message)
    }

    /**
     * Passing the focus request to the viewer's control.
     */
    def setFocus() {
        viewer.getControl().setFocus()
    }
}

/**
 * Companion object defining the ID of the view as specified by the extension.
 *
 * @author Florian Brandherm
 */
object FindRealBugsReportView {
    /**
     * The ID of the view as specified by the extension.
     */
    val ID: String = "de.tud.cs.st.bat.findrealbugs.plugin.views.FindRealBugsReportView"
}
