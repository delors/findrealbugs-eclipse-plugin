/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin
package extension

import findrealbugs.FindRealBugs
import org.eclipse.core.runtime.Platform

/**
 * This object manages the analyses that are provided by Eclipse plugins.
 *
 * @author Florian Brandherm
 */
object ExtensionAnalyses {

    /**
     * analysesMap: mapping of analysis names to analysis objects
     * urlMap: mapping of analysis names to user documentation URLs
     */
    val (analysesMap, urlMap) = getExtensionAnalyses()

    type AnalysesMap = Map[String, FindRealBugs.Analysis]
    type UrlMap = Map[String, String]

    /**
     * Loads the analyses that are provided by other plugins via the extension point
     * "de.tud.cs.st.bat.findrealbugs.plugin.analysis".
     *
     * If the extension point was not used correctly, the corresponding analyses will not
     * be loaded and an error message will be posted in the Eclipse error log.
     *
     * @return pair of a mapping of analysis names to analysis objects and a mapping of
     * analysis names to user documentation URLs
     */
    private def getExtensionAnalyses(): (AnalysesMap, UrlMap) = {
        // Initialize maps
        var analysesMap = Map[String, FindRealBugs.Analysis]()
        var urlMap = Map[String, String]()

        // Acquire extension point
        val exReg = Platform.getExtensionRegistry()
        val exPoint =
            exReg.getExtensionPoint("de.tud.cs.st.bat.findrealbugs.plugin.analysis")
        if (exPoint != null) {
            // Iterate through all extensions for this extension point
            for {
                extension ← exPoint.getExtensions()
                confElem ← extension.getConfigurationElements()
            } {
                val name = confElem.getAttribute("name")
                val url = confElem.getAttribute("userDocURL")
                try {
                    // Try creating the analysis object
                    val analysis = confElem.createExecutableExtension("class")
                    analysesMap += (name -> analysis.asInstanceOf[FindRealBugs.Analysis])
                    if (url != null) {
                        urlMap += (name -> url)
                    }
                } catch {
                    case e: Exception ⇒
                        val activator = Activator.getDefault
                        // Post error message to error log:
                        activator.logError("could not create analysis: class "+
                            confElem.getAttribute("class"), e)
                }
            }
        }

        (analysesMap, urlMap)
    }

    /**
     * Returns the names and their corresponding analyses that are introduced by other
     * plugins.
     *
     * @return Mapping of analysisNames to analysis objects
     */
    def analyses: AnalysesMap = analysesMap

    /**
     * Checks if an analysis name is part of the analyses that are introduced by other
     * plugins.
     *
     * @param analysisName name of the analysis in question
     * @return true, if the analysis is introduced by an other plugin, false otherwise
     */
    def containsAnalysis(analysisName: String): Boolean = {
        analysesMap.contains(analysisName)
    }

    /**
     * Retrieves the URL to the user documentation for a given analysis, if possible.
     * Be aware that not every analysis may have a documentation URL!
     *
     * @param analysisName name of the analysis, for which the URL is queried
     * @return Option that contains an URL, if it is known or None, if not
     */
    def getDocumentationURL(analysisName: String): Option[String] = {
        urlMap.get(analysisName)
    }
}
