/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin

import extension.ExtensionAnalyses
import org.osgi.framework.BundleContext
import org.eclipse.ui.plugin.AbstractUIPlugin
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.core.runtime.{ Status, IStatus }

/**
 * Activator for the Eclipse FindREALBugs plugin.
 *
 * This class handles the global initialization and cleanup of the plugin. It will be
 * called by the Eclipse Platform.
 *
 * @author Florian Brandherm
 */
class Activator extends AbstractUIPlugin {

    /**
     * Starts the plugin. Is called by Eclipse.
     *
     * @param context osgi-framework context of the plugin
     */
    override def start(context: BundleContext) {
        if (context != null) {
            super.start(context)
        }
        Activator.plugin = this
    }

    /**
     * Stops the plugin. Is called by Eclipse.
     *
     * @param context osgi-framework context of the plugin
     */
    override def stop(context: BundleContext) {
        Activator.plugin = null
        if (context != null) {
            super.stop(context)
        }
    }

    /**
     * Writes an error message to the error log.
     *
     * @param msg error message
     */
    def logError(msg: String) {
        logError(msg, null)
    }

    /**
     * Writes an error message to the error log.
     *
     * @param msg error message
     * @param e Exception that caused the error
     */
    def logError(msg: String, e: Exception) {
        getLog().log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.OK, msg, e))
    }
}

object Activator {
    /**
     * Plugin's ID
     */
    val PLUGIN_ID = "de.tud.cs.st.bat.findrealbugs.plugin"

    /**
     * Singleton pointer for this plugin
     */
    private var plugin: Activator = null

    /**
     * Returns the default activator for this plugin.
     *
     * @return the singleton instance of this plugin.
     */
    def getDefault: Activator = plugin
}
