/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin
package handlers

import extension.ExtensionAnalyses
import findrealbugs.FindRealBugsPluginInterface
import findrealbugs.plugin.views.FindRealBugsReportView
import properties.ProjectConfigurationPropertyPage
import org.eclipse.core.commands._
import org.eclipse.core.resources._
import org.eclipse.core.runtime._
import org.eclipse.core.runtime.jobs.Job
import org.eclipse.jdt.core._
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.viewers._
import org.eclipse.swt.widgets.Shell
import org.eclipse.ui._
import org.eclipse.ui.handlers.HandlerUtil
import java.net.URL

/**
 * Handles the analysis of Java projects.
 * It will be called by Eclipse when the user invoked an analysis.
 *
 * @author Florian Brandherm
 */
class AnalyzeHandler extends AbstractHandler {

    /**
     * Executes the analysis if the event that invoked it is valid. Only `IJavaProject`s
     * are valid selections for our analysis. Every other kind of selection will be
     * ignored with a Message Box stating that analyses can only be run on Java Projects.
     *
     * @param event The event that triggered this method
     * @return always null (unused, but the interface requires a return value)
     */
    def execute(event: ExecutionEvent): Object = {
        val shell: Shell = HandlerUtil.getActiveShell(event)
        val window: IWorkbenchWindow = HandlerUtil.getActiveWorkbenchWindowChecked(event)
        val sel: ISelection = HandlerUtil.getActiveMenuSelection(event)
        val selection: IStructuredSelection = sel.asInstanceOf[IStructuredSelection]

        // Check, what kind of element this execution is triggered on
        selection.getFirstElement() match {
            // If we have a java project, analyze it
            case project: IJavaProject ⇒ analyzeJavaProject(shell, window, project)
            // If we don't have a java project, complain to the user
            case x ⇒ MessageDialog.openInformation(
                window.getShell(),
                "FindREALBugs",
                "Analyses can only be run on Java Projects!")
        }

        return null
    }

    /**
     * Returns a given projects' class paths, specifically:
     *  1) The projects' class files
     *  2) Any referenced jar files (if they are not part of a library)
     *  3) The class paths of projects the given project depends on
     *
     * @param project The project whose class paths are requested.
     * @param alreadyVisitedProjects `Set` of all projects that were previously visited.
     * This ensures termination in the presence of cyclic project dependencies.
     * @return A `Set` containing all relevant class paths.
     */
    private def getJavaProjectClasspaths(
        project: IJavaProject,
        alreadyVisitedProjects: Set[IJavaProject] = Set()): Set[String] = {

        // Path to current workspace:
        // will be used to convert workspace-relative paths to absolute paths.
        val workspacePath: IPath = ResourcesPlugin.getWorkspace.getRoot.getLocation

        // Resolved class path:
        // contains only references to source folders, .jar files and projects.
        val resolvedClasspath = project.getJavaProject().getResolvedClasspath(true)

        // This set will contain the complete class path
        var classpaths = scala.collection.mutable.Set[String]()

        // Iterate through every class path entry
        for (classpathEntry ← resolvedClasspath) {
            val packageFragmentsRoots = project.findPackageFragmentRoots(classpathEntry)

            // If the class path entry points to a location outside the workspace,
            // it is considered external.
            val isExternal: Boolean =
                packageFragmentsRoots.length <= 0 ||
                    packageFragmentsRoots(0).isExternal()

            classpathEntry.getEntryKind() match {
                // For sources, add their respective output locations (folders) to the
                // class paths.
                case IClasspathEntry.CPE_SOURCE ⇒
                    val outputLocation = classpathEntry.getOutputLocation()
                    classpaths +=
                        (if (outputLocation != null) {
                            if (isExternal) {
                                outputLocation.toOSString
                            } else {
                                project.getProject.getLocation.append(
                                    outputLocation.removeFirstSegments(1)).toOSString
                            }
                        } else {
                            //If the entry contains no output location, it utilizes the
                            //projects default output location:
                            project.getProject.getLocation.append(
                                project.getOutputLocation.removeFirstSegments(1))
                                .toOSString
                        })

                // Include library files in the class paths
                case IClasspathEntry.CPE_LIBRARY ⇒
                    classpaths +=
                        (if (isExternal) {
                            classpathEntry.getPath().toString
                        } else {
                            project.getProject.getLocation.append(
                                classpathEntry.getPath.removeFirstSegments(1))
                                .toOSString
                        })

                // A projects' class paths will be obtained recursively
                case IClasspathEntry.CPE_PROJECT ⇒
                    // Find the project that corresponds to the referenced file path
                    val projectName = classpathEntry.getPath().segment(0).toString
                    val projects = ResourcesPlugin.getWorkspace.getRoot.getProjects
                    val projectOption = projects.find(_.getName() == projectName)

                    // Acquire its class paths (only if it's a Java project)
                    if (projectOption.isDefined) {
                        val projectDep = projectOption.get
                        if (projectDep.
                            isNatureEnabled("org.eclipse.jdt.core.javanature")) {
                            val javaProject = JavaCore.create(projectDep)
                            if (!alreadyVisitedProjects.contains(javaProject)) {
                                classpaths ++=
                                    getJavaProjectClasspaths(javaProject,
                                        alreadyVisitedProjects + project)
                            }
                        }
                    }
            }
        }

        // Return an immutable version of the set
        classpaths.toSet[String]
    }

    /**
     * Invokes a FindREALBugs analysis on a given Java project
     *
     * @param shell The active `Shell`
     * @param window The active `WorkbenchWindow`
     * @param project The `IJavaProject` that should be analyzed
     */
    private def analyzeJavaProject(
        shell: Shell,
        window: IWorkbenchWindow,
        project: IJavaProject) {

        // Contains all analyses that are added by an Eclipse plugin via the extension
        // point de.tud.cs.st.bat.findrealbugs.plugin.analysis
        val extensionAnalyses = ExtensionAnalyses.analyses

        /**
         * The analysis procedure is run in a separate user job which communicates back
         * it's progress. This reduces the perceived time taken by the analysis and allows
         * background processing without blocking the IDE.
         *
         * Running this process will result in a progress bar to be displayed.
         */
        val job: Job = new Job("FindREALBugs analysis") {
            // For user jobs, a progress bar will pop up
            setUser(true)

            /**
             * Performs the analysis in an other thread.
             * Note: all GUI-manipulation must be executed in the UI-thread!
             *
             * @param monitor Progress monitor that the job reports to. This handles the
             * progress bar.
             */
            protected def run(monitor: IProgressMonitor): IStatus = {
                val disabledAnalyses: Iterable[String] =
                    ProjectConfigurationPropertyPage.getDisabledAnalysesNames(project)

                // Set total number of work units. One work unit corresponds to one
                // analysis.
                monitor.beginTask("Analysis",
                    FindRealBugs.builtInAnalyses.size +
                        extensionAnalyses.size -
                        disabledAnalyses.size)
                monitor.subTask("Loading Java bytecode...")

                // Determine class paths
                val classpaths: Set[String] = getJavaProjectClasspaths(project)

                try {

                    /**
                     * Listens to the analysis state and updates the progress bar and
                     * it's accompanying text.
                     */
                    val progressListener = new ProgressListener {

                        /**
                         * holds the names of all currently running analyses
                         */
                        val currentAnalyses = scala.collection.mutable.TreeSet[String]()

                        /**
                         * If an analysis is started, add it to the list of currently
                         * running analyses and show that on the text underneath the
                         * progress bar.
                         *
                         * @param name The analysis' name.
                         * @param position The analysis' start number.
                         */
                        override def beginAnalysis(name: String, position: Integer) {
                            currentAnalyses += name
                            updateProgressBarText
                        }

                        /**
                         * If an analysis is completed, it is removed from the list of
                         * currently running analyses and the progress bar is
                         * incremented by one work unit.
                         *
                         * @param name The analysis' name.
                         * @param results The reports produced by the analysis, if any.
                         * @param position The analysis' start number.
                         */
                        override def endAnalysis(
                            name: String,
                            results: FindRealBugs.AnalysisReports,
                            position: Integer) {
                            monitor.worked(1)
                            currentAnalyses -= name
                            updateProgressBarText
                        }

                        /**
                         * Builds the string that is displayed underneath the
                         * progress bar and updates it.
                         */
                        private def updateProgressBarText {
                            val strCurrentAnalyses = currentAnalyses.mkString(", ")
                            if (monitor.isCanceled()) {
                                monitor.subTask("Finishing "+strCurrentAnalyses)
                            } else {
                                monitor.subTask(strCurrentAnalyses)
                            }
                        }

                        /**
                         * Checks if the user has cancelled the analysis process. Also
                         * shows this to the user.
                         *
                         * @return true, if the user cancelled the analysis, false
                         * otherwise
                         */
                        override def isCancelled: Boolean = {
                            val isCancelled = monitor.isCanceled()
                            if (isCancelled) {
                                updateProgressBarText
                            }
                            isCancelled
                        }
                    }

                    val (libraryClassPaths, projectClassPaths) =
                        classpaths.partition(_.endsWith(".jar"))

                    // Analyze
                    val unfilteredResultsArray =
                        FindRealBugsPluginInterface.runAnalysis(
                            projectClassPaths,
                            libraryClassPaths,
                            disabledAnalyses, progressListener,
                            extensionAnalyses)

                    monitor.subTask("Finishing")

                    // Filter out all reports concerning library (.jar) files
                    // TODO remove the need for this filtering
                    val resultsArray = (for {
                        (analysisName, reports) ← unfilteredResultsArray
                    } yield {
                        (
                            analysisName,
                            for {
                                report ← reports
                                if (!report.source.getOrElse(new URL("")).toString().
                                    contains(".jar"))
                            } yield {
                                report
                            }
                        )
                    }).filter(_._2.size > 0)

                    // Open view in UI thread
                    shell.getDisplay().syncExec(new Runnable() {
                        override def run() {
                            window.getActivePage().showView("de.tud.cs.st.bat."+
                                "findrealbugs.plugin.views.FindRealBugsReportView")
                            val view: FindRealBugsReportView = window.getActivePage().
                                findView("de.tud.cs.st.bat.findrealbugs.plugin.views."+
                                    "FindRealBugsReportView").
                                asInstanceOf[FindRealBugsReportView]
                            // Insert results into view
                            view.setResults(resultsArray, project)
                        }
                    })

                    Status.OK_STATUS

                } catch {
                    // If an error occurred during the analysis, display the error message
                    // in the UI thread
                    case e: FindRealBugsPluginInterface.FindRealBugsException ⇒
                        shell.getDisplay().syncExec(new Runnable() {
                            override def run() {
                                MessageDialog.openInformation(
                                    window.getShell(),
                                    "FindREALBugs",
                                    "error:\n"+e.getMessage())
                            }
                        })
                        Status.CANCEL_STATUS
                }
            }
        }

        job.schedule()
    }
}
