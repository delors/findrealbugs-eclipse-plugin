/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin
package views

import resolved.analyses.Severity
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jface.viewers._
import org.eclipse.jface.viewers.Viewer
import org.eclipse.swt.graphics.Image
import org.eclipse.ui.ide.IDE
import org.eclipse.ui.ISharedImages
import org.eclipse.ui.PlatformUI
import scala.collection.mutable.ListMap

/**
 * This class provides the content for the viewer. The viewer uses this to get access to
 * the user-defined tree structure.
 *
 * In this case the tree has three levels:
 *  1. On the first level there are IJavaProjects.
 *  2. On the second level there are tuples of a project and the analysis names as
 *     Strings: (IJavaProject,String).
 *  3. On the third level there are tuples of a project and ReportData objects:
 *     (IJavaProject,ReportData).
 *
 * This class is mutable.
 *
 * @author Florian Brandherm
 */
class ReportViewContentProvider
        extends IStructuredContentProvider with ITreeContentProvider {

    /**
     * Maps projects to maps that map analysis names to `Iterable`s of `ReportData`
     */
    var projects = new ListMap[IJavaProject, Map[String, Iterable[ReportData]]]()

    /**
     * Sets or updates the analysis results for a given `IJavaProject`.
     *
     * @param project The Java project that was analyzed.
     * @param results The analysis results as tree structure: array of an analysis name
     * with an array containing the reports data.
     */
    def setResults(
        project: IJavaProject,
        results: Array[(String, Iterable[ReportData])]) {
        val resultsMap = results.toMap
        projects += (project -> resultsMap)
    }

    /**
     * Returns all nodes of the first level (all analysis names).
     *
     * @param parent Unused, but required by interface
     * @return Array of analysis names (as Objects)
     */
    override def getElements(parent: Object): Array[Object] = {
        projects.keys.toArray
    }

    /**
     * Returns the parent nodes of a given node.
     *
     * @param child node as Object
     * @return parent node as Object (null, if no parent exists)
     */
    override def getParent(child: Object): Object = {
        child match {
            case project: IJavaProject                         ⇒ null
            case (project: IJavaProject, analysisName: String) ⇒ project
            case (project: IJavaProject, reportData: ReportData) ⇒
                getAnalysisName(project, reportData.asInstanceOf[ReportData])
        }
    }

    /**
     * Returns all children of a given node.
     *
     * @param parent parent node as Object
     * @return array of child nodes as Objects (can be empty)
     */
    def getChildren(parent: Object): Array[Object] = {
        parent match {
            case project: IJavaProject ⇒
                projects(project).keys.map { (project, _) }.toArray
            case (project: IJavaProject, analysisName: String) ⇒
                projects(project)(analysisName).map { (project, _) }.toArray
            case (project: IJavaProject, reportData: ReportData) ⇒ new Array[Object](0)
        }
    }

    /**
     * Checks whether a given node has children.
     *
     * @param parent the parent node as Object
     * @return true, if the node has children, false if not
     */
    def hasChildren(parent: Object): Boolean = {
        getChildren(parent).nonEmpty
    }

    /**
     * Returns the analysis name the given report belongs to.
     *
     * @param report The report for which the corresponding analysis name is requested.
     * @return The analysis name for the report.
     */
    def getAnalysisName(project: IJavaProject, report: ReportData): String = {
        val parent = projects(project).find(_._2.exists(_ == report))
        if (parent.isDefined) {
            parent.get._1
        } else {
            null
        }
    }

    /**
     * Unused, but required by the interface.
     */
    def inputChanged(v: Viewer, oldInput: Object, newInput: Object) {
    }

    /**
     * Unused, but required by the interface.
     */
    def dispose() {
    }

    /**
     * Unused, but required by the interface.
     */
    private def initialize() {
    }
}

/**
 * This class translates tree nodes from class ReportViewContentProvider into labels
 * and icons for display in the viewer.
 *
 * @author Florian Brandherm
 */
class ReportViewLabelProvider extends LabelProvider {

    /**
     * Returns the text that should be displayed in the tree view for a given node.
     *
     * Project nodes: project name
     * Analysis nodes: analysis name
     * `ReportData` nodes: formatted string (<classname>: <linenumber>: <message>)
     *
     * The caller is responsible that no other type of node is given.
     *
     * @param node The node for which the displayed text should be returned.
     * @return String that corresponds to the node.
     */
    override def getText(node: Object): String = {
        node match {
            case (_: IJavaProject, analysisName: String) ⇒ analysisName
            case project: IJavaProject                   ⇒ project.getProject().getName()
            case (_: IJavaProject, report: ReportData)   ⇒ report.label
        }
    }

    /**
     * Returns the icon that should be displayed for a given node.
     *
     * Project nodes: IDE's project symbol (usually a blue folder, but this can vary
     *                depending on the IDE)
     * Analysis nodes: a folder icon
     * Report nodes: severity icon, showing the report's severity
     *
     * The caller is responsible that no other type of node is given.
     *
     * @param node The node for which the icon should be determined.
     * @return Image that should be displayed for the node.
     */
    override def getImage(node: Object): Image = {
        val workbenchSharedImages = PlatformUI.getWorkbench().getSharedImages()
        node match {
            case (_: IJavaProject, analysisName: String) ⇒
                workbenchSharedImages.getImage(ISharedImages.IMG_OBJ_FOLDER)
            case project: IJavaProject ⇒
                workbenchSharedImages.getImage(IDE.SharedImages.IMG_OBJ_PROJECT)
            case (_: IJavaProject, report: ReportData) ⇒ workbenchSharedImages.getImage(
                report.severity match {
                    case Severity.Error   ⇒ ISharedImages.IMG_OBJS_ERROR_TSK
                    case Severity.Warning ⇒ ISharedImages.IMG_OBJS_WARN_TSK
                    case Severity.Info    ⇒ ISharedImages.IMG_OBJS_INFO_TSK
                    // Just to be safe:
                    case null             ⇒ ISharedImages.IMG_ETOOL_HOME_NAV
                })
        }
    }
}
