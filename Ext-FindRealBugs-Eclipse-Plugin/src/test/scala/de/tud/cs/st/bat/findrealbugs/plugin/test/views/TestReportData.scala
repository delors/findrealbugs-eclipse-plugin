/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin
package test
package views

import plugin.views._
import resolved._
import resolved.analyses._

/**
 * Tests for `ReportData`.
 *
 * @author Florian Brandherm
 */
@org.junit.runner.RunWith(classOf[org.scalatest.junit.JUnitRunner])
class TestReportData extends PluginTest {

    behavior of "ReportData"

    it should "construct from a ClassBasedResult correctly" in {
        val testReport = ClassBasedReport(None,
            Severity.Warning,
            ObjectType.Object,
            "testMessage")
        val result = ReportData(testReport, null)
        result should be(new ReportData("java.lang.Object", Severity.Warning,
            "class java.lang.Object: testMessage"))
    }

    it should "construct from a MethodBasedResult correctly" in {
        val testReport = MethodBasedReport(None,
            Severity.Info,
            ObjectType.Object,
            MethodDescriptor.NoArgsAndReturnVoid,
            "testMethod",
            "testMessage")
        val result = ReportData(testReport, null)
        result should be(new ReportData("java.lang.Object", Severity.Info,
            "in class java.lang.Object: method testMethod: testMessage"))

    }

    it should "construct from a FieldBasedResult correctly" in {
        val testReport = FieldBasedReport(None,
            Severity.Error,
            ObjectType.Object,
            Some(IntegerType),
            "testField",
            "testMessage")
        val result = ReportData(testReport, null)
        result should be(new ReportData("java.lang.Object", Severity.Error,
            "in class java.lang.Object: field testField: testMessage"))
    }

    it should "construct from a LineAndColumnBasedReport correctly if line and"+
        "column given" in {
            val testReport = LineAndColumnBasedReport(None,
                Severity.Warning,
                ObjectType.Object,
                MethodDescriptor.NoArgsAndReturnVoid,
                "testMethod",
                Some(42),
                Some(21),
                "testMessage")
            val result = ReportData(testReport, null)
            result should be(new ReportData("java.lang.Object", Severity.Warning,
                "in class java.lang.Object at line 42:21: testMessage") {
                override def linenumber = Some(42)
            })
        }

    it should "construct from a LineAndColumnBasedReport correctly if only line is"+
        "given" in {
            val testReport = LineAndColumnBasedReport(None,
                Severity.Warning,
                ObjectType.Object,
                MethodDescriptor.NoArgsAndReturnVoid,
                "testMethod",
                Some(42),
                None,
                "testMessage")
            val result = ReportData(testReport, null)
            result should be(new ReportData("java.lang.Object", Severity.Warning,
                "in class java.lang.Object at line 42: testMessage") {
                override def linenumber = Some(42)
            })

        }

    it should "construct from a LineAndColumnBasedReport correctly if neither line nor "+
        "column is given" in {
            val testReport = LineAndColumnBasedReport(None,
                Severity.Warning,
                ObjectType.Object,
                MethodDescriptor.NoArgsAndReturnVoid,
                "testMethod",
                None,
                None,
                "testMessage")
            val result = ReportData(testReport, null)
            result should be(new ReportData("java.lang.Object", Severity.Warning,
                "in class java.lang.Object: testMessage"))
        }

    //Test equals method

    it should "recognize ReportData objects as equal, that have equal values in their "+
        "fields and an equal line number" in {
            val reference = new ReportData("SampleName", Severity.Info,
                "You don't read this anyway.") {
                override def linenumber = Some(42)
            }
            reference should be(new ReportData("SampleName", Severity.Info,
                "You don't read this anyway.") {
                override def linenumber = Some(42)
            })
        }

    it should "recognize ReportData objects as equal, that have equal values in"+
        " their fields but only one has a line number" in {
            val reference = new ReportData("SampleName", Severity.Info,
                "You don't read this anyway.") {
                override def linenumber = Some(42)
            }
            reference should be(new ReportData("SampleName", Severity.Info,
                "You don't read this anyway."))
        }

    it should "recognize ReportData objects as not equal, that have different"+
        " messages" in {
            val reference = new ReportData("SampleName", Severity.Info,
                "You don't read this anyway.")
            reference should not be (new ReportData("SampleName", Severity.Info,
                "You don't read this anyway, but maybe it is important."))
        }
}
